Micro 1.x for Drupal 7.x
------------------------

Micro is the ultimate lightweight, fieldable entity type for when you 
just want a "fieldable thing" with no default special properties. The
only out-of-the-box properties of micro entities are the entity id
('mid') and bundle ('type') properties.

Features
--------

The functionality of some of the other properties of familiar entity 
types (eg. nodes) can be implemented using various core and contrib 
field types:

- You can give your micro types a title using core's Text field 
type.
- Date (Unix timestamp) fields can be configured to behave like the 
'created' property in Nodes, etc.
- Relationships to other entities are implemented using Entity 
Reference fields.
- Entity Reference fields targeting the user entity type can be 
configured to behave like the 'author' property in Nodes, etc.

Entity references from micro entities can be listed as:

- A page optionally associated with the target entity as a tab (menu 
local task) and/or as a link displayed as a pseudo-field in the target
entity content.
- A block displayed in a page region as usual, or as a pseudo-field in 
the target entity content.

Use Cases
---------

- User comments.
- Hierarchical structures (Artist -> Albums -> Songs -> Comments...)
- Facebook/Yammer styled wall.

Required Modules
----------------

- Entity API (http://drupal.org/project/entity)

Recommended Modules
-------------------

- Entity Reference (http://drupal.org/project/entityreference)
- Date (http://drupal.org/project/date)

Usage
-----

- Enable Micro and Micro Admin. (The latter is a separate module to 
allow Features/distribution developers to ship micro types minus the 
admin UI.)
- Create micro types at admin/structure/micro.
- Administer micro content at admin/content/micro.

You May Also Like
-----------------

- Entity Construction Kit (http://drupal.org/project/eck) is much more 
full-featured and flexible.
- Field Collection (http://drupal.org/project/field_collection) and 
Relation (http://drupal.org/project/relation) are more targeted to 
particular use cases.
